// HW ADVANCHED JS 
//  Асинхронність у JavaScript означає  що код може виконуватися без очікування завершення певної операції. Замість того, щоб чекати, коли буде готовий результат, 
// JavaScript може продовжувати виконувати інші дії.

document.addEventListener('DOMContentLoaded', () => {
    const findIPButton = document.getElementById('findButton');
    const resultDiv = document.getElementById('result');
  
    findIPButton.addEventListener('click', () => {
      try {
        getClientIP()
          .then(clientIP => getLocationInfo(clientIP))
          .then(locationInfo => displayLocationInfo(locationInfo))
          .catch(error => {
            console.error('Произошла ошибка:', error);
          });
      } catch (error) {
        console.error('Произошла ошибка:', error);
      }
    });
  
    function getClientIP() {
      return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://api.ipify.org/?format=json');
        xhr.onload = () => {
          if (xhr.status === 200) {
            const data = JSON.parse(xhr.responseText);
            resolve(data.ip);
          } else {
            reject(`Ошибка получения IP: ${xhr.status}`);
          }
        };
        xhr.send();
      });
    }
  
    function getLocationInfo(ip) {
      return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', `https://ip-api.com/json/${ip}`);
        xhr.onload = () => {
          if (xhr.status === 200) {
            const data = JSON.parse(xhr.responseText);
            resolve({
              continent: data.continent,
              country: data.country,
              region: data.regionName,
              city: data.city,
              district: data.district
            });
          } else {
            reject(`Ошибка получения инфы: ${xhr.status}`);
          }
        };
        xhr.send();
      });
    }
  
    function displayLocationInfo(locationInfo) {
      resultDiv.innerHTML = `
        <p>Континент: ${locationInfo.continent}</p>
        <p>Країна: ${locationInfo.country}</p>
        <p>Регіон: ${locationInfo.region}</p>
        <p>Місто: ${locationInfo.city}</p>
        <p>Район: ${locationInfo.district}</p>
      `;
    }
  });
  